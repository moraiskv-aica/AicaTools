<!-- ----------------------------------------------------  -->
# Aica - Tools

Este repositório contém o conjunto de softwares essenciais ao funcionamento do processador. Estão disponíveis aqui ferramentas para seu desenvolvimento por meio de programas de bootloader, GNU link script, ou então escrita de softwares do usuário em C ou ASM. Os diretórios e arquivos contém as seguintes finalidades:

- **bootloader**:           programa de bootloader do processador;
- **include**:              headers para utilização em programas C e ASM;
- **link**:                 GNU link script para mapeamento dos programas;
- **template**:             contém exemplos em C e ASM sobre como utilizar os headers, e acessar as funções do processador, e também modelos de makefile para a compilação de programas;
- **test**:                 conjunto de códigos teste em ASM, utilizado para testar o hardware durante simulações e uso prático.

<!-- ----------------------------------------------------  -->
# Instalação - Linux

<!-- ----------  -->
## Ferramentas GNU

Primeiro deve ser realizada a instalação do conjunto de ferramentas [GNU do RISC-V](https://github.com/riscv/riscv-gnu-toolchain). O repositório contém instruções sobre o processo de instalação das ferramentas, porém vale ressaltar que as mesmas devem ser compiladas para a arquitetura alvo do processador, RV32I, com ABI **ilp32**. Também deve ser estabelecida uma variável para acesso ao diretório de instalação das mesmas, sendo que será utilizado em conjunto com os softwares do processador.

O comando abaixo demonstra como se pode ser realizada a compilação das ferramentas para a arquitetura, que irá instalar então o compilador utilizado pelo comando **riscv32-unknown-elf-gcc**. Notando que $RISCV representa o caminho de instalação.

``` shell
./configure --prefix=$RISCV --with-arch=rv32i --with-abi=ilp32
make
```

**Versões das ferramentas:**

- Versão 8.3 do RISC-V GCC utilizada;
- Versão 2.2 do Manual de Nível de Usuário, e 1.10 do Manual de Nível Privilegiado da ISA do RISC-V.

**Observações:**

- as versões dos manuais do RISC-V já foram atualizadas, sendo encontradas as versões mais antigas no repositório [riscv-isa-manual](https://github.com/riscv/riscv-isa-manual/releases/tag/archive).

<!-- ----------------------------------------------------  -->
# Repositório

<!-- ----------  -->
## Bootloader

Programa responsável por inicializar o endereço do stack pointer (registrador x2 do banco), desabilitar qualquer interrupção do núcleo nos CSRs, e também nos periféricos. Sua função principal que realiza essas tarefas é escrita em ASM. Já a função responsável por carregar um código de formato ihex pela UART, para armazenar na memória principal, é escrito em C.

O bootloader deve ser síntetizado diretamente nos blocos de RAM (BRAMs) que formam a cache do processador, no momento de implementação do hardware. Como consequência, o tamanho do programa de boot deve se restringir ao total de 16kB.

<!-- ----------  -->
## Include

Conjunto de header C/ASM, sendo o único essencial para uso do processador o **<aica_io.h>**, com definições como o endereço inicial do Stack Pointer e também de dispositivos I/O. Para uso do mesmo tanto em códigos C quanto ASM, os arquivos assembly a serem compilados (que fizerem uso do header), devem ser de extensão **.S**.

Finalidade dos header:

- **<aica_io>**: MACROS essencial para escrita de programas destinadas ao processador, contém informações básicas como o endereço em que se localiza dispositivos I/O, e o tamanho das regiões de memória (texto, dados, etc). Também tem a definição do endereço inicial do stack pointer, e o tamanho da cache L1 disponível no hardware. Disponível para uso na linguagem C e ASM;
- **<csr.h>**: MACROS de acesso para leitura/escrita dos CSR do núcleo, disponível para uso apenas na linguagem C;
- **<trap_handler.h>**: MACROS para declarar funções de tratamento de interrupções/exceções, contendo também uma MACRO que define uma função principal do trap handler que utiliza o modo DIRECT do CSR mtvec do núcleo. Disponível para uso apenas na linguagem C;
- **<utils_uart.h>**: MACROS de acesso para leitura/escrita da UART, disponível para uso apenas na linguagem C.

<!-- ----------  -->
## Link

GNU link script, responsável por atribuir as instruções e dados as suas respectivas seções da memória. Também define o local das tabelas de vetores de exceções e interrupções.

<!-- ----------  -->
## Template

O diretório template contém pequenos exemplos em C, do uso das bibliotecas escritas para acesso as funcionalidades do processador. Como por exemplo, leitura de registradores de controle, ou então a declaração das rotinas de exceção/interrupção.

Também é disponibilizado dois modelos de projetos inciais em C e ASM, **asm_project** e **c_project**. Ambos com um arquivo Makefile já configurado. É importante notar que o Makefile não incluí o caminho para os header, e nem para o script de link. Sendo assim, é necessário indicar dentro do mesmo os caminhos, ou então copiá-los para o diretório do projeto criado. Neste último caso, os headers podem ser copiados para dentro de **_include_** (o Makefile verifica as bibliotecas contidas dentro), e o script de link no mesmo diretório do Makefile.

<!-- ----------  -->
## Teste

**Descrito dentro de seu próprio diretório.**
